Test parser

Description:   parser pet project
Technology:   Python
Essence:         Web site with which you can trade cars

parser site https://www.kijiji.ca/b-apartments-condos/city-of-toronto/c37l1700273

Installation
1 Clone from repository              git clone <ssh>
2 Go to project folder               cd <repo>
3 Install venv, if it not installed  pip install virtualenv
4 Create new venv                    virtualenv <name>
5 Activate venv                      source venv/bin/activate
6 Install all required moduls        pip install -r requirements.txt
7 using create_base.py file create a table
8 database backup in parser.backup file
9 site parser in file main.py
